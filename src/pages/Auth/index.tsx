import React, { useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import { Button } from 'antd'
import { useDispatch, useSelector } from 'react-redux'
import { loginRequest } from './duck/AuthSlice'
import { ApplicationState } from '../../app/store'
import { notificationError } from '../../components/Notification'

function Login() {
  const { isSignedIn, error } = useSelector((state: ApplicationState) => state.auth)
  const history = useHistory()
  const dispatch = useDispatch()

  const handleLogin = () => {
    const data = {
      email: 'tenant1@email.com',
      password: '123456789',
    }
    dispatch(loginRequest(data))
  }

  useEffect(() => {
    if (isSignedIn) {
      history.push('/')
    }
  }, [isSignedIn, history])

  return (
    <div>
      {error && notificationError(error)}
      <Button onClick={handleLogin}>Login</Button>
    </div>
  )
}

export default Login
