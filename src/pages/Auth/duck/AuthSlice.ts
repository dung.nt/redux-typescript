import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { login } from './services'
import { getToken } from '../../../utils/common'
import { keyNames } from '../../../constants/defaultValues'

export interface UserRequest {
  email: string
  password: string
}

export const loginRequest = createAsyncThunk('user/login', async (user: UserRequest) => {
  const response = await login(user)
  return response.data.data
})

export interface AuthState {
  isSignedIn: boolean
  error: ''
}

const initialState = getToken() ? { isSignedIn: true, error: '' } : { isSignedIn: false, error: '' }

const slice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    logout: (state) => {
      localStorage.removeItem(keyNames.token)
      state.isSignedIn = false
      state.error = ''
    },
  },
  extraReducers: {
    [loginRequest.rejected.toString()]: (state, action) => {
      state.isSignedIn = false
      state.error = action.error.message
    },
    [loginRequest.fulfilled.toString()]: (state, action) => {
      localStorage.setItem(keyNames.token, action.payload.token_info.token)
      state.isSignedIn = true
      state.error = ''
    },
  },
})

export const { logout } = slice.actions

export default slice.reducer
