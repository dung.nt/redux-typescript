import axiosClient from '../../../utils/axiosClient'
import { UserRequest } from './AuthSlice'

export const login = (body: UserRequest) => {
  return axiosClient.post('/login', body)
}
