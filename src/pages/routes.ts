import { lazy, ComponentType } from 'react'
import iconDashboard from '../assets/icons/icon.png'

const Dashboard = lazy(() => import('./Dashboard'))
const DashboardCEO = lazy(() => import('./Dashboard/DashboardCEO'))
const OpsDailyReport = lazy(() => import('./Dashboard/OpsDailyReport'))
const Customers = lazy(() => import('./Customers'))

export interface DefaultRouteComponent {
  path: string
  name: string
  exact: boolean
  icon: string
  component: ComponentType
}

export default [
  {
    path: '/dashboard',
    exact: true,
    name: 'Dashboard',
    icon: iconDashboard,
    component: Dashboard,
    children: [
      {
        path: '/dashboard',
        name: 'Dashboard',
        component: Dashboard,
      },
      {
        path: '/dashboard/ceo',
        name: 'Dashboard CEO',
        component: DashboardCEO,
      },
      {
        path: '/dashboard/daily',
        name: 'Ops Daily Report',
        component: OpsDailyReport,
      },
    ],
  },
  {
    path: '/customers',
    exact: true,
    name: 'Customers',
    icon: iconDashboard,
    component: Customers,
  },
]
