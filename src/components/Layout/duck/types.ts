// Example for using discriminated union types.
export type ThemColors = 'light' | 'dark'

// Use enums for better autocompletion of action type names. These will
// be compiled away leaving only the final value in your compiled code.
export enum LayoutActionTypes {
  SET_THEME = '@@layout/SET_THEME',
}

// Declare state types with `readonly` modifier to get compile time immutability.
export interface LayoutState {
  readonly theme: ThemColors
}
