import { LayoutActionTypes, ThemColors } from './types'

export const setTheme = (theme: ThemColors) => {
  return {
    type: LayoutActionTypes.SET_THEME,
    payload: theme,
  }
}
