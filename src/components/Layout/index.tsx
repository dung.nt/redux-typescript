import React, { Suspense } from 'react'
import { injectIntl } from 'react-intl'
import { Switch, Route, Redirect, RouteProps } from 'react-router-dom'
import { ThemeProvider } from 'styled-components'
import { Layout, Spin } from 'antd'
import LayoutContainer from '../../containers/LayoutContainer'
import * as themes from '../../styles/theme'
import { GlobalStyles } from '../../styles/global'
import './Layout.scss'
import Sidebar from './Sidebar'
import SubSidebar from './SubSidebar'
import MainHeader from './Header'
import routes from '../../pages/routes'

const { Content, Footer } = Layout

const DefaultLayout = ({ intl }: any) => {
  return (
    <LayoutContainer>
      {({ theme, setTheme }) => (
        <ThemeProvider theme={themes[theme]}>
          <GlobalStyles />
          <Layout style={{ minHeight: '100vh' }}>
            <Redirect from="/" to="/dashboard" />
            <Sidebar />
            <SubSidebar />
            <Layout className="site-layout">
              <MainHeader />
              <Content style={{ margin: '0 16px' }}>
                <Suspense fallback={<Spin />}>
                  <Switch>
                    {routes.map((route) => {
                      const { component: Component }: RouteProps = route
                      return (
                        <Route
                          key={route.path}
                          path={route.path}
                          exact={route.exact}
                          render={(props) => <Component {...props} />}
                        />
                      )
                    })}
                    {routes.map((route) => {
                      const { children } = route
                      return (
                        children &&
                        children.map((child) => {
                          const { component: ChildComponent }: RouteProps = child
                          return (
                            <Route
                              key={child.path}
                              path={child.path}
                              exact
                              render={(props) => <ChildComponent {...props} />}
                            />
                          )
                        })
                      )
                    })}
                  </Switch>
                </Suspense>
              </Content>
              <Footer className="text-center">
                {intl.formatMessage({ id: 'app.name' })} ©2020 Created by Developer Team
              </Footer>
            </Layout>
          </Layout>
        </ThemeProvider>
      )}
    </LayoutContainer>
  )
}

export default injectIntl(DefaultLayout)
