import React from 'react'
import { Layout } from 'antd'
import imgLogo from '../../../assets/logo.png'
import SubMenu from './SubMenu'

const { Header, Content } = Layout

const SubSidebar = () => {
  return (
    <Layout className="site-layout sub-sidebar">
      <Header className="site-layout-background" style={{ padding: 0 }}>
        <div className="logo">
          <img src={imgLogo} alt="Logo" />
        </div>
      </Header>
      <Content>
        <SubMenu />
      </Content>
    </Layout>
  )
}

export default SubSidebar
