import React, { memo } from 'react'
import { Link, useLocation } from 'react-router-dom'
import { FormattedMessage } from 'react-intl'
import routes, { DefaultRouteComponent } from '../../../../pages/routes'

const SubMenu = () => {
  const location = useLocation()
  const currentRoute = routes.find((item) => location.pathname.includes(item.path))
  const children: any = currentRoute?.children

  return (
    <>
      {children && (
        <>
          <h4 className="text-center">
            <FormattedMessage id={currentRoute ? currentRoute.name : ''} />
          </h4>
          <ul>
            {children.map((item: DefaultRouteComponent) => (
              <li key={item.path}>
                <Link to={item.path}>
                  <FormattedMessage id={item.name} />
                </Link>
              </li>
            ))}
          </ul>
        </>
      )}
    </>
  )
}

export default memo(SubMenu)
