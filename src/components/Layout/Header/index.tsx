import React from 'react'
import { Layout, Row, Col, Badge, Popover, Menu, Dropdown } from 'antd'
import { DownOutlined } from '@ant-design/icons'
import './Header.scss'
import { useDispatch } from 'react-redux'
import SearchBox from '../../SearchBox'
import iconMenu from '../../../assets/icons/icon.png'
import { logout } from '../../../pages/Auth/duck/AuthSlice'

const { Header } = Layout

const content = (
  <div>
    <p>Content</p>
    <p>Content</p>
  </div>
)

const MainHeader = () => {
  const dispatch = useDispatch()

  const menu = (
    <Menu onClick={() => dispatch(logout())}>
      <Menu.Item key="1">Sign Out</Menu.Item>
    </Menu>
  )

  return (
    <Header className="site-layout-background" style={{ padding: 0 }}>
      <Row style={{ width: '100%' }}>
        <Col id="search-box" xs={24} lg={12}>
          <SearchBox style={{ width: '200px' }} />
        </Col>
        <Col xs={24} lg={12} className="menu-wrapper">
          <ul className="menu-list">
            <li className="menu-list__item">
              <Popover placement="bottomRight" content={content} trigger="click">
                <Badge dot>
                  <img src={iconMenu} alt="Menu" className="menu__icon" />
                </Badge>
              </Popover>
            </li>
            <li className="menu-list__item">
              <Popover placement="bottomRight" content={content} trigger="click">
                <Badge count={5}>
                  <img src={iconMenu} alt="Menu" className="menu__icon" />
                </Badge>
              </Popover>
            </li>
            <li className="menu-list__item">
              <Popover placement="bottomRight" content={content} trigger="click">
                <Badge count={11}>
                  <img src={iconMenu} alt="Menu" className="menu__icon" />
                </Badge>
              </Popover>
            </li>
            <li className="menu-list__item">
              <Popover placement="bottomRight" content={content} trigger="click">
                <Badge count={3}>
                  <img src={iconMenu} alt="Menu" className="menu__icon" />
                </Badge>
              </Popover>
            </li>
          </ul>
          <Dropdown overlay={menu}>
            <a
              href="/"
              className="ant-dropdown-link menu-profile"
              onClick={(e) => e.preventDefault()}
            >
              <img src={iconMenu} alt="Profile" className="menu-profile__avatar" />
              <span style={{ display: 'flex', flexDirection: 'column', paddingLeft: '10px' }}>
                <span>
                  Hey, <strong>Brother</strong>
                </span>
                <span>
                  Admin <DownOutlined />
                </span>
              </span>
            </a>
          </Dropdown>
          <Popover placement="bottomRight" content={content} trigger="click">
            <img src={iconMenu} alt="Menu" className="menu__icon" />
          </Popover>
        </Col>
      </Row>
    </Header>
  )
}

export default MainHeader
