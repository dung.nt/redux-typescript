import React, { useState } from 'react'
import { FormattedMessage } from 'react-intl'
import { Link } from 'react-router-dom'
import { Menu, Layout } from 'antd'
import { FileOutlined } from '@ant-design/icons'
import routes from '../../../pages/routes'

interface IconProps {
  source: string
}

const { Sider } = Layout

const IconMenu = ({ source }: IconProps) => (
  <span className="anticon anticon-file">
    <img src={source} alt="icon" style={{ width: '14px' }} />
  </span>
)

function Sidebar() {
  const [collapsed, setCollapsed] = useState(false)

  const onCollapse = () => {
    setCollapsed(!collapsed)
  }

  return (
    <Sider collapsible collapsed={collapsed} onCollapse={onCollapse}>
      <div className="logo" />
      <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
        <Menu.Item key="9" icon={<FileOutlined />}>
          Files
        </Menu.Item>
        {routes.map((item) => (
          <Menu.Item key={item.path} icon={<IconMenu source={item.icon} />}>
            <Link to={item.path}>
              <FormattedMessage id={item.name} />
            </Link>
          </Menu.Item>
        ))}
      </Menu>
    </Sider>
  )
}

export default Sidebar
