import { notification } from 'antd'

export const notificationError = (message: string) => {
  notification.error({
    message: 'Error',
    description: message,
  })
}

export const notificationSuccess = (message = 'Success!') => {
  notification.success({
    message: 'Success',
    description: message,
  })
}
