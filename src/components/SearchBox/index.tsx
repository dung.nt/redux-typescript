import React, { CSSProperties } from 'react'
import { Input } from 'antd'

const { Search } = Input

interface SearchProps {
  style: CSSProperties
}

const SearchBox = (props: SearchProps) => {
  const { style } = props

  const onSearch = (value: string) => console.log(value)

  return <Search placeholder="input search text" onSearch={onSearch} style={style} />
}

export default SearchBox
