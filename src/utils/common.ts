import { keyNames } from '../constants/defaultValues'

export const getToken = () => {
  return localStorage.getItem(keyNames.token)
}
