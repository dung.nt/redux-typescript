import { createSlice } from '@reduxjs/toolkit'
import { locales } from '../constants/defaultValues'

export interface LocaleState {
  currentLocale: string
}

const locale = createSlice({
  name: 'locale',
  initialState: { currentLocale: locales.en },
  reducers: {
    switchLanguage: (state, action) => {
      state.currentLocale = action.payload
    },
  },
})

const { actions, reducer } = locale
export const { switchLanguage } = actions
export default reducer
