import { combineReducers } from 'redux'
import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit'
import { all } from 'redux-saga/effects'
import createSagaMiddleware from 'redux-saga'
import { LayoutState } from '../components/Layout/duck/types'
import { layoutReducer } from '../components/Layout/duck/reducer'
import localeReducer, { LocaleState } from './LocaleSlice'
import authReducer, { AuthState } from '../pages/Auth/duck/AuthSlice'

const sagaMiddleware = createSagaMiddleware()

const middleware = [...getDefaultMiddleware(), sagaMiddleware]

const rootReducer = combineReducers({
  layout: layoutReducer,
  locale: localeReducer,
  auth: authReducer,
})

function* rootSaga() {
  yield all([
    // authSaga(),
    // contractSaga(),
    // setupSaga()
  ])
}

const store = configureStore({
  reducer: rootReducer,
  middleware,
})

sagaMiddleware.run(rootSaga)

export interface ApplicationState {
  layout: LayoutState
  locale: LocaleState
  auth: AuthState
}

export default store
