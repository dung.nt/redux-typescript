import React, { lazy, Suspense } from 'react'
import { IntlProvider } from 'react-intl'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { Spin } from 'antd'
import './App.css'
import './styles/scss/style.scss'
import PrivateRoute from './components/PrivateRoute'
import messagesEn from './translations/en.json'
import messagesAr from './translations/ar.json'
import { locales } from './constants/defaultValues'
import { ApplicationState } from './app/store'

const Login = lazy(() => import('./pages/Auth'))
const Error404 = lazy(() => import('./pages/Error404'))
const DefaultLayout = lazy(() => import('./components/Layout'))

function App() {
  const { locale, auth } = useSelector((state: ApplicationState) => state)
  const { currentLocale } = locale
  const messages = currentLocale === locales.en ? messagesEn : messagesAr
  const { isSignedIn } = auth

  return (
    <IntlProvider locale={currentLocale} messages={messages}>
      <Router>
        <Suspense fallback={<Spin />}>
          <Switch>
            <Route exact path="/login" component={Login} />
            <PrivateRoute path="/" isSignedIn={isSignedIn} component={DefaultLayout} />
            <Route component={Error404} />
          </Switch>
        </Suspense>
      </Router>
    </IntlProvider>
  )
}

export default App
