import { createGlobalStyle } from 'styled-components'

export const GlobalStyles = createGlobalStyle`
    *,
    *::after,
    *::before {
        box-sizing: border-box
    }
    
    body {
        align-items: center;
        background: ${({ theme }) => JSON.parse(JSON.stringify(theme)).colors.background};
        color: ${({ theme }) => JSON.parse(JSON.stringify(theme)).colors.body};
    }
`
