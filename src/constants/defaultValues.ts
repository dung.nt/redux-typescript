export const locales = {
  all: ['en', 'ar'],
  en: 'en',
  ar: 'ar',
}

export const listThemes = {
  all: ['dark', 'light'],
  dark: 'dark',
  light: 'light',
}

export const keyNames = {
  token: 'token',
}
