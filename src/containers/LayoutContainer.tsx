import { FC, ReactElement } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { ThemColors } from '../components/Layout/duck/types'
import * as layoutActions from '../components/Layout/duck/actions'
import { ApplicationState } from '../app/store'

// Re-dux specific props.
interface LayoutContainerProps {
  theme: ThemColors
  setTheme: (theme: ThemColors) => void
}

// Wrapper props for render/children callback
interface LayoutContainerRenderProps {
  render?: (props: LayoutContainerProps) => ReactElement
  children?: (props: LayoutContainerProps) => ReactElement
}

const LayoutContainer: FC<LayoutContainerRenderProps> = ({ render, children }) => {
  const { theme } = useSelector((state: ApplicationState) => state.layout)
  const dispatch = useDispatch()

  const setTheme = (color: ThemColors) => dispatch(layoutActions.setTheme(color))

  if (render) {
    return render({ theme, setTheme })
  }

  if (children) {
    return children({ theme, setTheme })
  }

  return null
}

export default LayoutContainer
