module.exports = {
    semi: false,
    singleQuote: true,
    trailingComma: 'all',
    allowParens: 'avoid',
    printWidth: 100,
    tabWidth: 2
}
