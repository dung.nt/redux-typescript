const APP_NAME = 'MT6_Admin';
const PATH = '/home/dabank/mt6/admin';
const USER = 'dabank';

const PROD_IP = '206.189.149.77';
const PROD_PORT = '2323';
const PROD_BRANCH = 'origin/master';

const DEV_IP = '206.189.43.65';
const DEV_PORT = '2323';
const DEV_BRANCH = 'origin/dev';

const STG_IP = '178.128.91.106'
const STG_PORT = '2323'
const STG_BRANCH = 'origin/staging'

const REPO = 'git@gitlab.com:fomo-bank/mt6-admin.git';

const PRE_DEPLOY = 'git checkout .';

const POST_DEPLOY = {
    PROD:
        'ln -nfs ../shared/.env .env && npm install && npm run build && pm2 reload ecosystem.config.js --env production',
    DEV:
        'ln -nfs ../shared/.env .env && npm install && npm run build && pm2 reload ecosystem.config.js --env dev'
};

module.exports = {
    apps: [
        {
            name: APP_NAME,
            script: './.start.sh',
            env_production: {
                REACT_APP_NODE_ENV: 'production'
            },
            env_dev: {
                REACT_APP_NODE_ENV: 'development'
            }
        }
    ],

    deploy: {
        prod: {
            user: USER,
            host: [
                {
                    host: PROD_IP,
                    port: PROD_PORT
                }
            ],
            ref: PROD_BRANCH,
            repo: REPO,
            path: PATH,
            'post-deploy': POST_DEPLOY.PROD
        },
        stg: {
            user: USER,
            host: [{
                host: STG_IP,
                port: STG_PORT
            }],
            ref: STG_BRANCH,
            repo: REPO,
            path: PATH,
            'post-deploy': POST_DEPLOY.PROD
        },
        dev: {
            user: USER,
            host: [
                {
                    host: DEV_IP,
                    port: DEV_PORT
                }
            ],
            ref: DEV_BRANCH,
            repo: REPO,
            path: PATH,
            'pre-deploy': PRE_DEPLOY,
            'post-deploy': POST_DEPLOY.DEV
        }
    }
};
